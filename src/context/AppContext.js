import { createContext, useReducer } from "react";
import { GET_ANNOUNCEMENTS, SET_USER, SET_CURRENT_ANNOUNCEMENT } from "./actions";
const initialAppState = {
  announcements:[],
  currentUser:null,
  isLogin: false,
  currentAnnouncement: null
}

const AppContext = createContext(initialAppState);
const { Provider } = AppContext;



const StateProvider = ( { children } ) => {
  const [state, dispatch] = useReducer((state, action) => {
    switch(action.type) {
      case GET_ANNOUNCEMENTS:
        return {...state, announcements: action.payload}
      case SET_USER:
        return {...state, currentUser: action.payload}
      case SET_CURRENT_ANNOUNCEMENT:
        return {...state, currentAnnouncement: action.payload}
      default:
        throw new Error();
    };
  }, initialAppState);

  return <Provider value={{ state, dispatch }}>{children}</Provider>;
};

export { AppContext, StateProvider }
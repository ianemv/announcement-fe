import React, { useState, useEffect } from 'react'
import Announcement from '../../components/Annoucement/Announcement'
import Loading from '../../components/Loading/Loading';
import apiService from '../../services/apiService';
import './landing-page.css';

const LandingPage = () => {

  const [announcements, setAnnouncements] = useState(null)
  const [loading, setLoading] = useState(true)


  const fetchAnnouncements = async () =>{
    const results = await apiService.get('announcements?active=true');
    setAnnouncements(results.data);
    setLoading(false);
  }


  useEffect(() => {
    fetchAnnouncements();
  },[]);

  const renderAnnouncements = () => {
    return announcements.map((a,i) =>{
      return <Announcement announcement={a} key={`a-${i}`}/>
     })
  }

  return (
    <div id="LandingPage">
      <div className="container">
        {
          loading && <Loading/>
        }
        { announcements && renderAnnouncements()}
        { announcements?.length == 0 && <div>No announcements yet</div>}
      </div>
    </div>
  )
}

export default LandingPage

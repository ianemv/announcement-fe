import React, { useContext, useEffect, useState } from 'react'
import { useForm } from "react-hook-form";
import { authService } from '../../services/auth';
import './login.css'
import { AppContext } from '../../context/AppContext'
import { SET_USER } from '../../context/actions';
import { Link, useHistory } from "react-router-dom";
import SmallLoader from '../../components/SmallLoader/SmallLoader';



const LoginPage = () => {

  const { register, handleSubmit, watch, formState: { errors } } = useForm();
  const [loading, setLoading] = useState(false);
  const [loginError, setLoginError] = useState(false);

  const appState = useContext(AppContext);
  const { dispatch } = appState;
  const { currentUser } = appState.state;

  const history = useHistory();

  
  const onSubmit = async (data) => {
    setLoading(true);
    const result = await authService.login({email:data.email,password:data.password});    
    
    if (result?.error){
      setLoginError(true);
      setLoading(false);
      return;
    }
    
    if (result){
      authService.setUser(result);
      dispatch({type: SET_USER, payload: result })
      setLoading(false);
      
      setTimeout(() => {
       history.push("/");        
      }, 500);
    }
  }

  useEffect(() => {
    
  }, [currentUser])


  return (
    <div id="Login">
      <h2>Login</h2>
      <div className="form-wrapper">
      <form onSubmit={handleSubmit(onSubmit)}>
            <label id="email">Email</label>
            <input type="email" htmlFor="email" {...register("email", { required: true })} disabled={loading}/>
            <label id="password">Password</label>
            <input type="password"  {...register("password")} htmlFor="password" disabled={loading}/>
            <button type="submit" disabled={loading}> {loading && <SmallLoader/>} Login</button>
        </form>
        {loginError && <div className="error">Email or password did match.</div>}
        <Link to="/">Home</Link>
      </div>
    </div>
  )
}

export default LoginPage

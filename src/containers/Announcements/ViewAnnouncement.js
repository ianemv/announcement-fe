import React, { useContext } from 'react'
import { Link } from 'react-router-dom';
import Announcement from '../../components/Annoucement/Announcement';
import { AppContext } from '../../context/AppContext';
import './view.css'

const ViewAnnouncement = () => {
  
  const appState = useContext(AppContext);
  const { currentAnnouncement } = appState.state;

  return (
    <div id="ViewAnnouncement">
      <Announcement announcement={currentAnnouncement} showEdit={true}/>
    </div>
  )
}

export default ViewAnnouncement

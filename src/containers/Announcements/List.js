import React, { useContext, useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import Loading from '../../components/Loading/Loading'
import SmallLoader from '../../components/SmallLoader/SmallLoader'
import { SET_CURRENT_ANNOUNCEMENT } from '../../context/actions'
import { AppContext } from '../../context/AppContext'
import ApiService from '../../services/apiService'
import { authService } from '../../services/auth'
import './list.css'

const ListAnnouncements = () => {
  const history = useHistory();


  const [announcements, setAnnouncements] = useState([])
  const [showConfirmDelete, setConfirmDelete] = useState(false)
  const [deleteId, setDeleteId] = useState(null)
  const [loading, setLoading] = useState(false)

  const appState = useContext(AppContext);
  const { dispatch } = appState;
  const { currentUser } = appState.state;

  const fetchAnnouncements = async () =>{
    setLoading(true)
    const results = await ApiService.get('announcements');    
    setAnnouncements(results.data);
    setLoading(false);
  }

  useEffect(() => {
    fetchAnnouncements();    
  }, []);

  const editAnnouncement =(data)  =>{    
    dispatch({type:SET_CURRENT_ANNOUNCEMENT, payload: data })
    history.push("/admin/announcements/edit/"+data.id);
  }
  const viewAnnouncement =(data)  =>{    
    dispatch({type:SET_CURRENT_ANNOUNCEMENT, payload: data })
    history.push("/admin/announcements/view/"+data.id);
  }

  const deleteAnnouncement = async ()  =>{    
    
    if (deleteId == null) return;

    setLoading(true);
    const resp = await ApiService.del({url:"announcements/"+deleteId});    
    setLoading(false);
    
    if (resp.error){
      authService.logout();
      history.push("/");        
    }

    setDeleteId(null);
    setConfirmDelete(false);
    
    fetchAnnouncements();
  }

  const toggleConfirmDelete = (id) => {
    setDeleteId(id);
    setConfirmDelete(true);
  }

  const renderRows = () =>{
    return announcements.map((a,i) =>{
      
      return <tr key={`row-${i}`}>
            <td>{a.title}</td>
            <td>{a.content}</td>
            <td>{a.active == 0 ? 'Inactive' : 'Active'}</td>
            <td>{a.startDate}</td>
            <td>{a.endDate}</td>
            <td className="text-center">
              <button className="btn"  onClick={() => viewAnnouncement(a)}>View</button>
              <button className="btn" onClick={() => editAnnouncement(a)}> Edit</button>
              <button className="btn" onClick={() => toggleConfirmDelete(a.id) }>Delete</button>
            </td>
        </tr>
     })
  }
  

  return (
    <div id="List">      
      <Link className="btn link-btn" to="/admin/announcements/add">Add Announcement</Link>
      <table id="ListTable">
        <thead>
          <tr>
            <th>Title</th>
            <th>Content</th>
            <th>Active</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Action</th>
          </tr>
        </thead>
        
        <tbody>
        { loading ?
          <tr className="loader">
            <td colSpan="6">
            <SmallLoader/>
            </td>
          </tr>
          :
            renderRows()
        }
        </tbody>
      </table>
      {showConfirmDelete &&
        <>
        <div className="modal">
          <div className="confirm">
            <h3>Are you sure you want to delete this announcement?</h3>
            <div>
              <button className="btn" onClick={deleteAnnouncement}>Yes</button>
              <button className="btn" onClick={()=>setConfirmDelete(false)}>No</button>
            </div>
          </div>
        </div>
        </>
      }
    </div>
  )
}

export default ListAnnouncements

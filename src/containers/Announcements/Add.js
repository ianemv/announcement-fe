import React from 'react'
import AnnouncementForm from './Form'

const AddAnnouncement = () => {
  return (
    <div>
      <AnnouncementForm/>
    </div>
  )
}

export default AddAnnouncement

import React, { useContext } from 'react'
import { AppContext } from '../../context/AppContext';
import AnnouncementForm from './Form'

const EditAnnouncement = () => {


  const appState = useContext(AppContext);
  const { dispatch } = appState;
  const { currentAnnouncement } = appState.state;

  return (
    <div>
      <AnnouncementForm announcement={currentAnnouncement}/>
    </div>
  )
}

export default EditAnnouncement

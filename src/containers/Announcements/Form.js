import React, { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form';
import { useHistory } from "react-router-dom";
import ApiService from '../../services/apiService';
import { authService } from '../../services/auth';
import './form.css'

const AnnouncementForm = ({announcement}) => {
  
  const { register, handleSubmit, setValue, formState: { errors } } = useForm();
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const onSubmit = async (data) => {
    
    //data.active = parseInt(data.active) == 0 ? true : false;

    if (data.active === "0"){
      data.active = true;
    }

    console.log(data);
    //return;
    
    const resp = await ApiService.post({url:"announcements", body: data, secured:true});    
    setLoading(true);
    
    if (resp.error){
      authService.logout();
      history.push("/");        
    }

    setTimeout(() => {
      history.push("/admin/announcements");        
    }, 500);
  }

  useEffect(() => {
    if (announcement) {
      //setValue([announcement]);
      console.log(announcement)
    }
  }, [announcement]);

  return (
    <div id="AnnouncementForm">
      <form onSubmit={handleSubmit(onSubmit)}>
        <label id="title">Title</label>
        <input type="text" htmlFor="title" {...register("title", { required: true })} 
            defaultValue={announcement?.title}
            disabled={loading}/>
          {errors.title && errors.title.type === "required" && (
          <span role="alert"  className="error">This is required</span>
          )}

        <label id="content">Content</label>
        <textarea type="text" htmlFor="content" {...register("content", { required: true })} 
              disabled={loading}
              defaultValue={announcement?.content}
              rows="4" cols="50"/>
       {errors.content && errors.content.type === "required" && (
          <span role="alert"  className="error">This is required</span>
          )} 
        <label id="startDate">Start Date</label>
        <input type="date" htmlFor="startDate" {...register("startDate", { required: true })} 
          defaultValue={announcement?.startDate}
          disabled={loading}/>
        {errors.startDate && errors.startDate.type === "required" && (
          <span role="alert"  className="error">This is required</span>
          )}
        
        <label id="endDate">End Date</label>
        <input type="date" htmlFor="endDate" {...register("endDate", { required: true })}
          defaultValue={announcement?.endDate}
          disabled={loading}/>
         {errors.endDate && errors.endDate.type === "required" && (
          <span role="alert" className="error">This is required</span>
          )} 
        
        <label id="active">active
        </label>
          <input type="checkbox" htmlFor="active" {...register("active")}
            disabled={loading}
            defaultValue={announcement?.active}
            checked={announcement?.active}

            />
      <input type="hidden" htmlFor="id" {...register("id")}
          defaultValue={announcement?.id}
          disabled={loading}/>
        <div className="btn-group">
          <button className="btn" type="submit">Save</button>
          <button className="btn" type="button" onClick={()=>history.push("/admin/announcements")}>Cancel</button>
        </div>
      </form>
      <div>
        
      </div>
    </div>
  )
}

export default AnnouncementForm

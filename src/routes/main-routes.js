import LandingPage from "../containers/LandingPage/LandingPage"


const routes = [
  {
      path: "/",
      component: LandingPage
  }
]
export default routes

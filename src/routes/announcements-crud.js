import AddAnnouncement from "../containers/Announcements/Add"
import EditAnnouncement from "../containers/Announcements/EditAnnouncement"
import ListAnnouncements from "../containers/Announcements/List"
import ViewAnnouncement from "../containers/Announcements/ViewAnnouncement"

const routes = [
  {
      path: "/admin/announcements/add",
      component: AddAnnouncement
  },
  {
      path: "/admin/announcements/edit/:id",
      component: EditAnnouncement
  },
  {
      path: "/admin/announcements/view/:id",
      component: ViewAnnouncement
  },
  {
      path: "/admin/announcements",
      component: ListAnnouncements
  }
]
export default routes

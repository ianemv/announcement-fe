import LoginPage from "../containers/Login/LoginPage"

const routes = [
  {
      path: "/login",
      component: LoginPage
  }
]
export default routes

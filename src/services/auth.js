import ApiService from './apiService'


const login = ({email, password}) => {
  const resp = ApiService.post({url: 'auth/login', body: {email, password}});
  return resp;
}

const setUser = (user) =>{
  localStorage.setItem('user', JSON.stringify(user));
}

const getUser = () => {
  let user = localStorage.getItem('user');
  return JSON.parse(user) || null;
}

const logout = () => {
  localStorage.clear();
}

export const authService = {
  login,
  logout,
  setUser,
  getUser
}

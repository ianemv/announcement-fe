import {API_URL} from '../utils/const'
import { authService } from './auth';

const get = (url, options) => {
  
    let myHeaders = new Headers();
    myHeaders.append('pragma', 'no-cache');
    myHeaders.append('cache-control', 'no-cache');

    var myInit = {
      method: 'GET',
      headers: myHeaders,      
    };



  return fetch(`${API_URL}/${url}`, myInit).then((data)=>{return data.json()})
        .then((data)=>{  
          return data;
        })
        .catch((e)=>{
          if(e.name === "AbortError") { }
          else {
            console.error(e)
          }
        }); 
}

const post = ( { url, body, options, secured }) => {
  let requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
  };

  if (secured){
    let user = authService.getUser();
    requestOptions.headers["Authorization"] = `Bearer ${user.access_token}`
  }
  
  requestOptions.body = JSON.stringify(body);

  return fetch(`${API_URL}/${url}`,requestOptions)
          .then(
            (response) => {
              if (!response.ok) {
                throw Error(response.statusCode);
              }
              return response.json()
            }
          )
          .then((data)=>{  						
						return data;
					})
					.catch((e)=>{
						if(e.name === "AbortError") { }
						else {
							console.error(e)
						}
            return {error:true, message:e};
					});
}

const del = ( { url, body, options }) => {
  let requestOptions = {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json' },
  };

    let user = authService.getUser();
    requestOptions.headers["Authorization"] = `Bearer ${user.access_token}`
  
  return fetch(`${API_URL}/${url}`,requestOptions)
          .then(
            (response) => {
              if (!response.ok) {
                throw Error(response.statusCode);
              }
              return response.json()
            }
          )
          .then((data)=>{  						
						return data;
					})
					.catch((e)=>{
						if(e.name === "AbortError") { }
						else {
							console.error(e)
						}
            return {error:true, message:e};
					});
}


const ApiService = {
  get,
  post,
  del
}

export default ApiService;

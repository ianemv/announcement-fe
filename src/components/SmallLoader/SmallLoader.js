import React from 'react'
import './small-loader.css';

const SmallLoader = () => {
  return (
    <div>
      <div className="lds-facebook"><div></div><div></div><div></div></div>
    </div>
  )
}

export default SmallLoader

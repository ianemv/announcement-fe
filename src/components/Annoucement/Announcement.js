import React from 'react'
import { Link } from 'react-router-dom';
import './annoucement.css';

const Announcement = ({announcement, showEdit}) => {
  const { title, content, startDate, endDate } = announcement;
  return (
    <div className="Announcement">
      <h2 className="heading">{title}</h2>
      <div className="inner">
          <span className="metadata">Starts On:{startDate}</span>
          <span className="metadata">End Date: {endDate}</span>
          <p>{content}</p>
      {
        showEdit &&
        <div className="mb-20">
          <Link  to={`/admin/announcements/edit/${announcement.id}`}>Edit</Link>
        </div>
      }
      </div>
    
    </div>
  )
}

export default Announcement

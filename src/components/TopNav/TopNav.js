import React, { useContext, useState } from 'react'
import { AppContext } from '../../context/AppContext';
import { authService } from '../../services/auth';
import { Link, useHistory } from "react-router-dom";
import './topnav.css';
import { SET_USER } from '../../context/actions';

const TopNav = () => {

  const [openMenu, setOpenMenu] = useState(false)

  const appState = useContext(AppContext)
  const { dispatch } = appState;
  const { currentUser } = appState.state;
  const history = useHistory();

  const logout = () =>{
    authService.logout();
    dispatch({type: SET_USER, payload: null});
    toggleMenu();
    history.push("/");   
  }

  const toggleMenu = () => {
    setOpenMenu(!openMenu);
  }

  const publicLinks = () =>{
    return (
      <>
        <li>
          <Link to="/login">Login</Link>
        </li>
      </>
    )
  }
  const privateLinks = () =>{
    return (
      <>
        <li><Link to="/">Home</Link></li>
        <li><Link to="/admin/announcements">Manage Announcements</Link></li>
        <li onClick={logout}>Logout</li>
      </>
    )
  }

  return (
    <div id="TopNav">
      <div className="burger-menu" id="burger-menu" onClick={toggleMenu}>
        <div></div>
        <div></div>
        <div></div>
      </div>
      
      <div className={ `navigation ${openMenu ? 'open' : 'close'} `  }>
        <ul>
          {!currentUser && publicLinks()}
          {currentUser && privateLinks()}
        </ul>
      </div>
    </div>
  )
}

export default TopNav

import React, { useEffect, useState } from 'react'
import { Redirect } from 'react-router';
import { authService } from '../../services/auth'
import Header from '../Header/Header'

const AdminLayout = () => {
  const [user, setUser] = useState({});


  useEffect(() => {
    const user = authService.getUser();

    if (user.access_token){
      setUser(user);
    }

  }, [])

  return (
    <div>
      <Header/>
      {children}
    </div>
  )

}

export default AdminLayout

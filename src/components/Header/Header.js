import React, { useContext } from 'react'
import { AppContext } from '../../context/AppContext';
import TopNav from '../TopNav/TopNav';
import './header.css';

const Header = () => {

  const appState = useContext(AppContext)
  const { dispatch } = appState;
  const { currentUser } = appState.state;


  return (
    <div id="Header">
       <h1>Suburb Announcements</h1>
       <div className="top-container">
       <TopNav/>
       </div>
       
    </div>
  )
}

export default Header

import React from 'react'
import SmallLoader from '../SmallLoader/SmallLoader'
import './loading.css'
const Loading = () => {
  return (
    <div id="Loading">
      <SmallLoader/>
    </div>
  )
}

export default Loading

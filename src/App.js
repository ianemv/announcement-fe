import { Route, BrowserRouter as Router, Switch, Redirect } from "react-router-dom";
import { createBrowserHistory } from "history";
import './App.css';

import MainRoutes from './routes/main-routes';
import LoginRoutes from './routes/login';
import AdminRoutes from './routes/announcements-crud';

import LoginLayout from "./components/layouts/LoginLayout";
import DefaultLayout from "./components/layouts/DefaultLayout";
import { authService } from "./services/auth";
import { useContext, useEffect } from "react";
import { AppContext } from "./context/AppContext";
import { SET_USER } from "./context/actions";


const history = createBrowserHistory();

const MainRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(matchProps) => (
        <DefaultLayout>
          <Component {...matchProps} />
        </DefaultLayout>
      )}
    />
  );
};
const LoginRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(matchProps) => (
        <LoginLayout>
          <Component {...matchProps} />
        </LoginLayout>
      )}
    />
  );
};
const PrivateRoute = ({ component: Component, ...rest }) => {

  const user = authService.getUser();

  return (
    <Route
      {...rest}
      render={({ matchProps }) =>
        user !== null ? (
          <DefaultLayout>
            <Component {...matchProps} />
          </DefaultLayout>
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              
            }}
          />
        )
      }
    />
  );
};

function App() {

  const appState = useContext(AppContext);
  const { dispatch } = appState;

  useEffect(() => {
    const user = authService.getUser();
    dispatch({type: SET_USER, payload: user });
  }, [])

  return (
    <div className="App">
      <Router history={history}>
        <Switch>
        {LoginRoutes.map((value, index) => {
              return (
                <LoginRoute
                  key={`auth-${index}`}
                  path={value.path}
                  component={value.component}
                  exact
                />
              );
            })}
         {MainRoutes.map((value, index) => {
              return (
                <MainRoute
                  key={index}
                  path={value.path}
                  component={value.component}
                  exact
                />
              );
            })}
         {AdminRoutes.map((value, index) => {
              return (
                <PrivateRoute
                  key={index}
                  path={value.path}
                  component={value.component}
                  exact
                />
              );
            })}

        </Switch>
      </Router>
    </div>
  );
}

export default App;
